window.gtpb = window.gtpb || {};



     

     


gtpb.default.site_id = "gt";
gtpb.default.SiteMenu = {"global":[{"URL":"/","Page":null,"Name":"Home","Menu":"global","Identifier":"tutorials","Pre":"","Post":"","Weight":1,"Parent":"","Children":null}],"iconmenu":[{"URL":"#","Page":null,"Name":"Facebook","Menu":"iconmenu","Identifier":"","Pre":"Facebook","Post":"","Weight":1,"Parent":"","Children":null}]};
gtpb.default.SiteParams = {"absolutebuilderurl":true,"accent_color":"hsl(5,39%,45%)","accent_color_light":"hsl(5,39%,65%)","description":"Delico SCM empowers you to manage supply chain where production takes place, by local experts.","env":"production","flex_box_interior_classes":"w-100 w-50-m w-33-l ","fullname":"Delico SCM Co Ltd","gh_provider":"gitlab","gh_repo_image":["images/architecture-1867187_1920_pixabay.jpg","images/logos/icon-on-dark.svg","images/logos/icon-on-light.svg","images/logos/logo.svg"],"gh_repo_name":"delico.gitlab.io","gh_repo_owner":"delico","global_section":{"default_footer":{"appearance":{"legal_link_color":"link-white-80","nav_link_color":"link-gold","section_background":"bg-black-80","text_color":"white-80"},"copyright":"Copyright 2019 Delico SCM Co Ltd. All rights reserved.","data_key":"default_footer","is_global":true,"menu":{"footer_legal":[{"name":"Terms","url":"#"}],"footer_nav":[{"name":"Home","url":"#top"}]},"partial":"goandtalk/footer/default_footer"}},"gt_author_name":"Goandmake SEO Marketing","gt_author_url":"https://we.goandmake.app","gt_license_name":"CC-BY-NC-4.0","gt_license_url":"https://creativecommons.org/licenses/by-nc/4.0/","gt_theme_name":"Golden","header_class":"bg-primary-color","include_favicon":true,"js_export":{},"js_partial":{},"lightness":45,"lightness_dark":20,"lightness_light":65,"lightness_lighter":80,"logo_url":"/images/logos/logo.svg","mainSections":["post"],"main_menu_align":"start","mainsections":["post"],"mobile_menu_button":[{"label":"Menu","menu":"global"},{"label":"Connect","menu":"iconmenu"}],"primary_hue":84,"saturation":48,"skip_cover_nav":false,"skip_custom_css_file":true,"skip_fa_icon":false};
gtpb.default.SiteBaseURL = "https://delico.gitlab.io/";
gtpb.default.SiteCopyright = "Copyright 2019, Delico SCM Co Ltd";
gtpb.default.SiteTitle = "Delico SCM";
gtpb.default.SiteFooter = {"appearance":{"legal_link_color":"link-white-80","nav_link_color":"link-gold","section_background":"bg-black-80"},"is_global":true,"menu":{"footer_legal":[{"name":"Terms of Service","url":"#"},{"name":"Privacy","url":"#"}],"footer_nav":[{"name":"Home","url":"#"},{"name":"About","url":"#"}]}};
